<?php
namespace Ecentura\NoFollowIndex\Plugin;

use Magento\Catalog\Helper\Product\View as ProductViewHelper;
use Magento\Framework\Registry;
use Magento\Framework\View\Page\Config as PageConfig;
use Magento\Catalog\Helper\Product as CatalogProduct;

class AfterPrepareAndRenderPlugin
{
    public const FOLLOW = 'FOLLOW';

    public const NOFOLLOW = 'NOFOLLOW';

    public const INDEX = 'INDEX';

    public const NOINDEX = 'NOINDEX';

    /**
     * @var CatalogProduct
     */
    protected $catalogProduct;
    /**
     * @var PageConfig
     */
    private $pageConfig;
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @param CatalogProduct $catalogProduct
     * @param PageConfig $pageConfig
     * @param Registry $registry
     */
    public function __construct(
        CatalogProduct  $catalogProduct,
        PageConfig $pageConfig,
        Registry $registry
    ) {
        $this->catalogProduct = $catalogProduct;
        $this->pageConfig = $pageConfig;
        $this->registry = $registry;
    }

    /**
     * Show follow, index attribute for catalog product view layout
     *
     * @param ProductViewHelper $subject
     * @param mixed $result
     * @return mixed
     */
    public function afterPrepareAndRender(ProductViewHelper $subject, $result): mixed
    {
        $product = $this->registry->registry('current_product');
        if ($product && $product->getData('follow_index_status')) {
            $follow = $product->getData('follow_value') ? self::FOLLOW : self::NOFOLLOW;
            $index = $product->getData('index_value') ? self::INDEX : self::NOINDEX;
            $this->pageConfig->setRobots($follow . ',' . $index);
        }
        return $result;
    }
}
