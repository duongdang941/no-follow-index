<?php

namespace Ecentura\NoFollowIndex\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class AddAttributeFollowIndex implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory          $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Add custom attributes follow, index to a product object
     *
     * @return void
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'follow_index_status',
            [
                'type' => 'varchar',
                'label' => 'Enable No Follow Index',
                'input' => 'boolean',
                'default' => '',
                'global' => ScopedAttributeInterface::SCOPE_WEBSITE,
                'visible' => true,
                'user_defined' => true,
                'required' => false,
                'used_in_product_listing' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'group' => 'No Follow Index',
                'is_used_in_grid' => true,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'follow_value',
            [
                'type' => 'int',
                'label' => 'Follow Value',
                'input' => 'select',
                'source' => \Ecentura\NoFollowIndex\Model\Config\Source\FollowValue::class,
                'default' => '',
                'global' => ScopedAttributeInterface::SCOPE_WEBSITE,
                'visible' => true,
                'user_defined' => true,
                'required' => false,
                'used_in_product_listing' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'group' => 'No Follow Index',
                'is_used_in_grid' => true,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'index_value',
            [
                'type' => 'int',
                'label' => 'Index Value',
                'input' => 'select',
                'source' => \Ecentura\NoFollowIndex\Model\Config\Source\IndexValue::class,
                'default' => '',
                'global' => ScopedAttributeInterface::SCOPE_WEBSITE,
                'visible' => true,
                'user_defined' => true,
                'required' => false,
                'used_in_product_listing' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'group' => 'No Follow Index',
                'is_used_in_grid' => true,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false
            ]
        );
    }

    /**
     *  Get the aliases for the data patch.
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get the dependencies for the data patch.
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
