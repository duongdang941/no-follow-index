<?php

namespace Ecentura\NoFollowIndex\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class FollowValue extends AbstractSource
{
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        return [
            ['value' => '1', 'label' => __('Follow')],
            ['value' => '0', 'label' => __('No Follow')],
        ];
    }

    /**
     * Get option text based on value
     *
     * @param string $value
     * @return string|null
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return null;
    }
}
